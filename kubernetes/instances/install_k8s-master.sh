#! /bin/bash
sudo su

swapoff -a

echo "br_netfilter ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs" > /etc/modules-load.d/k8s.conf
    
curl -fsSL https://get.docker.com | bash

apt-get update -y && apt-get install -y apt-transport-https ca-certificates curl

curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
            
apt-get update  && apt-get install -y kubelet kubeadm  kubectl

apt-mark hold kubelet kubeadm kubectl

kubeadm config images pull

kubeadm init

mkdir -p $HOME/.kube && cp -i /etc/kubernetes/admin.conf $HOME/.kube/config && chown ubuntu:ubuntu $HOME/.kube/config

kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

modprobe br_netfilter ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs

apt-get install bash-completion
echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc
echo "source <(kubectl completion bash)" >> /root/.bashrc
source /root/.bashrc

sudo su
git clone https://github.com/kodekloudhub/kubernetes-metrics-server.git

kubectl create -f kubernetes-metrics-server/


#su ubuntu
#mkdir -p $HOME/.kube && cp -i /etc/kubernetes/admin.conf $HOME/.kube/config && chown ubuntu:ubuntu $HOME/.kube/config





