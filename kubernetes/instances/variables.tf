variable "worker_instance_name"{
    default = "POC K8s Worker"
    type = string
    description = "Nome a ser colocado nas instancias criadas"


    validation{
        condition = length(var.worker_instance_name) > 3
        error_message = "Nome da imagem precisa ter pelo menos 3 caracteres." 
    }
}

variable "master_instance_name"{
    default = "POC K8s Master"
    type = string
    description = "Nome a ser colocado nas instancias criadas"


    validation{
        condition = length(var.master_instance_name) > 3
        error_message = "Nome da imagem precisa ter pelo menos 3 caracteres." 
    }
}

variable "main_vpc" {
    description = "The VPC ID"
    type = string
}

variable "aws_subnet" {
    description = "The subnet id"
    type        = string
}

variable "env"{
    type = string
}

variable "instances"{
    type = number    
}

variable "master_instances"{
    type = number    
}

variable "key_name" {
    type = string
    description = "nome do arquivo com a chave ssh"
}

variable "blocks"{
    type = list(object({
        device_name = string
        volume_size = number
        volume_type = string
    }))
}