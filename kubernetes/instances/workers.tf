resource "aws_instance" "workers" {
    count         = var.instances
    ami           = data.aws_ami.ubuntu.id
    instance_type = "t2.medium"
    security_groups = [aws_security_group.allow_ssh.id, aws_security_group.SG_docker-2.id, aws_security_group.SG_Kubernetes.id]
    key_name = var.key_name
    subnet_id = var.aws_subnet
    user_data = file("${path.module}/install_k8s.sh")

    dynamic "ebs_block_device" {
        for_each = var.blocks
        content{
        device_name = ebs_block_device.value["device_name"]
        volume_size = ebs_block_device.value["volume_size"]
        volume_type = ebs_block_device.value["volume_type"]
        }
    }

    tags = {
        Name = "%{if var.worker_instance_name == "Ricardo" } ${var.worker_instance_name} %{ else } POC - ${var.worker_instance_name} %{ endif }!"
        Env = var.env
        Numero = "VM - ${count.index}"
    }
}