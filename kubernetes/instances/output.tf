output "master_ip_address" {
  value = aws_instance.master.*.public_ip
}

output "master_privateip_ip_address" {
  value = aws_instance.master.*.private_ip
}

output "workers_ip_address" {
  value = aws_instance.workers.*.public_ip
}