variable "vpc_cidr_block" {
    description = "Range de IPs"
    type = string
    default     = "172.17.0.0/16"
}

variable "az_count" {
    description = "AZ numbers"
    type = number
    default = 1
}

