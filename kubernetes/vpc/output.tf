output "vpc_main" {
    value = aws_vpc.main.id
}

output "public_subnet" {
    value = aws_subnet.public.0.id
}
