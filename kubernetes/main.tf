provider "aws" {
  region = terraform.workspace == "default" ? "sa-east-1" : "us-east-1"
}