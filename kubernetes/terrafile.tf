module "vpc" {
  source        = "./vpc"
}

module "instances" {
    source              = "./instances"
    instances           = "${terraform.workspace == "default" ? 2 : 3}"
    master_instances    = 1
    env                 = "${terraform.workspace == "default" ? "default" : "prod"}"
    worker_instance_name = "worker"
    master_instance_name = "master"
    blocks        = [
        {
        device_name = "/dev/sdg"
        volume_size = 5
        volume_type = "gp2"
        },
        {
        device_name = "/dev/sdh"
        volume_size = 10
        volume_type = "gp2"
        }
    ]
    main_vpc = module.vpc.vpc_main
    aws_subnet = module.vpc.public_subnet
    key_name = "docker-aws"
}

output "k8s-master" {
  value = module.instances.master_ip_address
}

output "k8s-master-private-ip" {
  value = module.instances.master_privateip_ip_address
}

output "k8s-workers" {
    value = module.instances.workers_ip_address
}

