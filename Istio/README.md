Este Plano faz a criação dos seguintes itens:

- VPC
- Subnets
- Route Tables
- Elastic IPs
- NAT
- Security Groups para docker e kubernetes
- Instancias EC2 T3.small (não free tier)
- Instalação do Docker
- Instalação do Kubernetes

Nesta configuração o plano fica armazendo no S3, por isso no arquivo main.tf deve-se informar o nome do seu bucket no S3.
Para que o terraform consiga conectar no S3, siga as instruções desse link: https://www.terraform.io/docs/providers/aws/index.html

Para execução desse plano criei uma imagem que está no dockerfile, no docker hub está disponivel como ricardorm13/terraform:1.0

Execute # docker container run -it -v $PWD:/app_terraform ricardorm13/terraform:1.0 sh

Após isso já estará dentro do container no diretório com os arquivos do terraform.


