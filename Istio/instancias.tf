data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}


resource "aws_instance" "ricnet-aws-worker" { 
    count = var.instances_count      
    ami = data.aws_ami.ubuntu.id 
    instance_type = "t3.small" 
    security_groups = [aws_security_group.allow_ssh.id, aws_security_group.SG_docker-2.id, aws_security_group.SG_Kubernetes.id, aws_security_group.SG_Istio.id]
    key_name = "docker-aws"
    subnet_id = aws_subnet.public.0.id
    #user_data = file("install_k8s.sh")

    tags = {
        Name = "Ricnet-Worker-${count.index}"
    }

    
}

resource "aws_instance" "ricnet-aws-master" { 
    ami = data.aws_ami.ubuntu.id 
    instance_type = "t3.small" 
    security_groups = [aws_security_group.allow_ssh.id, aws_security_group.SG_docker-2.id, aws_security_group.SG_Kubernetes.id, aws_security_group.SG_Istio.id]
    key_name = "docker-aws"
    subnet_id = aws_subnet.public.0.id
    #user_data = file("install_k8s-master.sh")

    tags = {
        Name = "Ricnet-Master"
    }

    
}

output "k8s-master" {
  value = aws_instance.ricnet-aws-master.*.public_ip
}

output "k8s-master-private-ip" {
  value = aws_instance.ricnet-aws-master.*.private_ip
}

output "k8s-workers" {
  value = aws_instance.ricnet-aws-worker.*.public_ip
}
