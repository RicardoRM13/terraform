#! /bin/bash
sudo su

swapoff -a

echo "br_netfilter ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs" > /etc/modules-load.d/k8s.conf
    
curl -fsSL https://get.docker.com | bash

apt-get update -y && apt-get install -y apt-transport-https

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/k8s.list
            
apt-get update  && apt-get install -y kubelet kubeadm  kubectl

kubeadm config images pull

kubeadm init

mkdir -p /root/.kube && cp -i /etc/kubernetes/admin.conf /root/.kube/config && chown root:root /root/.kube/config

kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

modprobe br_netfilter ip_vs_rr ip_vs_wrr ip_vs_sh nf_conntrack_ipv4 ip_vs

git clone https://github.com/kodekloudhub/kubernetes-metrics-server.git

kubectl create -f kubernetes-metrics-server/

apt-get install bash-completion
echo "source /etc/profile.d/bash_completion.sh" >> ~/.bashrc
echo "source <(kubectl completion bash)" >> /root/.bashrc
source /root/.bashrc


#instalaço do Istio

curl -L https://git.io/getLatestIstio | ISTIO_VERSION=1.4.4 sh -

cd istio-1.4.4

export PATH=$PWD/bin:$PATH

for i in install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done





